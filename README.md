# vga-backlight-control
The purpose of this image is to control the backlight of a host server that does not have X11 installed. 

## Usage
The image can set the backlight either as a percentage from _0%_ to _100%_
```bash
docker run -v /sys:/sys:rw registry.gitlab.com/mbroen1/vga-backlight-control:latest 90%
```

or as a hexadecimal value from _00_ to _FF_
```bash
docker run -v /sys:/sys:rw registry.gitlab.com/mbroen1/vga-backlight-control:latest E6
```

Note that the `-v /sys:/sys:rw` is required to allow the image access to the PCI interface.

## Use Case
Running servers on hardware with integrated screens (laptops, etc.) without all the screens lighting up the room.

## TODO
* Support for machines with multiple VGA capable devices (currently the first found device is used)
