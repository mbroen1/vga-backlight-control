FROM alpine:3.8
COPY vga-set-backlight /
RUN apk update \
 && apk add pciutils bash
ENTRYPOINT ["/vga-set-backlight"]
CMD []
